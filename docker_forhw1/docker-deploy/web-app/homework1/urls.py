from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('myuser/', include('myuser.urls')),
    path('admin/', admin.site.urls),
]
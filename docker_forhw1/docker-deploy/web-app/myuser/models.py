from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from datetime import datetime
from django.contrib.postgres.fields import ArrayField

def emptylist():
    return [];

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    driver = models.BooleanField(default=False)
    psg = models.BooleanField(default=False)
    share = models.BooleanField(default=False)
    vehicle = models.CharField(max_length=20, blank=True)
    plate = models.CharField(max_length=10, blank=True)
    capacity = models.IntegerField(default=1, validators=[MaxValueValidator(100), MinValueValidator(1)])
    special = models.CharField(max_length=200, blank=True, default='')

class Trip(models.Model):  
    destination = models.CharField(max_length=50, blank=False)
    arrivaltime = models.DateTimeField()
    passenger = models.IntegerField(validators=[MaxValueValidator(100),MinValueValidator(1)])
    ifshare = models.BooleanField(default=False)
    vehicle = models.CharField(max_length=20, blank=True)
    d_id = models.IntegerField(default=-1, blank=True)
    p_id = models.IntegerField(default=-1, blank=False)
    status = models.CharField(max_length=10, default='open')
   
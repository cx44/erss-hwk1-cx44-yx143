from django.contrib import messages
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from .models import UserProfile, Trip
from django.contrib import auth
from .forms import RegForm, LoginForm,DriverForm,TripForm,UserEmailForm
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q



def home(request):
    return render(request, 'myuser/home.html')


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/myuser/")
    


def register(request):
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password2']

            user = User.objects.create_user(username=username, first_name=first_name, last_name=last_name, password=password, email=email)

            userprofile = UserProfile(user=user)
            userprofile.save()
            messages.success(request, 'Your account has been created. '
                             'You are now able to log in!')
            return HttpResponseRedirect("/myuser/login/")

    else:
        form = RegForm()

    return render(request, 'myuser/register.html', {'form':form})

# login into your account
def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = auth.authenticate(username=username, password=password)

            if user is not None and user.is_active:
                auth.login(request, user)
                messages.success(request, 'Login successfully!')
                return HttpResponseRedirect(reverse('myuser:profile', args=[user.id]))
            else:
                return render(request, 'myuser/login.html', {'form':form, 'message': 'Wrong password. Please try again.'})

    else:
        form = LoginForm()

    return render(request, 'myuser/login.html', {'form': form})
    
@login_required
def profile(request, id):
    user = get_object_or_404(User, id=id)
    userprofile = get_object_or_404(UserProfile, user=user)
    
    
#    tripasdriver= Trip.objects.filter(d_id=id)
#    tripaspsg= Trip. objects.filter(p_id=id)
    
#    context={
#    'tripasdriver':tripasdriver,
#    'tripaspsg':tripaspsg,
#    }
    return render(request, 'myuser/profile.html', {'user': user, 'userprofile':userprofile})
    


@login_required
def driverreg(request, id):
    user = get_object_or_404(User, id=id)
    userprofile = get_object_or_404(UserProfile, user=user)

    if request.method == 'POST':
        form = DriverForm(request.POST)

        if form.is_valid():
            userprofile.driver = True
            userprofile.vehicle = form.cleaned_data['vehicle']
            userprofile.plate = form.cleaned_data['plate']
            userprofile.capacity = form.cleaned_data['capacity']
            userprofile.save()

            return HttpResponseRedirect(reverse('myuser:profile', args=[user.id]))

    else:
        form = DriverForm()

    return render(request, 'myuser/driverreg.html', {'form': form, 'user': user})



@login_required
def editdriver(request, id):
    user = get_object_or_404(User, id=id)
    userprofile = get_object_or_404(UserProfile, user=user)

    if request.method == 'POST':
        form = DriverForm(request.POST)

        if form.is_valid():
            userprofile.vehicle = form.cleaned_data['vehicle']
            userprofile.plate = form.cleaned_data['plate']
            userprofile.capacity = form.cleaned_data['capacity']
            userprofile.save()
            messages.success(request, 'Successfully logged in!')
            return HttpResponseRedirect(reverse('myuser:profile', args=[user.id]))
    else:
        context = {
            'vehicle': userprofile.vehicle,
            'plate': userprofile.plate,
            'capacity': userprofile.capacity,
            'special': userprofile.special,
        }
        form = DriverForm(context)

        return render(request, 'myuser/editdriver.html', {'user': user, 'userprofile': userprofile, 'form': form})


@login_required
def newtrip(request, id):
    user = get_object_or_404(User, id=id)
    userprofile = get_object_or_404(UserProfile, user=user)
    
    if request.method == 'POST':
        form = TripForm(request.POST)

        if form.is_valid():
            destination = form.cleaned_data['destination']
            arrivaltime = form.cleaned_data['arrivaltime']
            passenger = form.cleaned_data['passenger']
            ifshare = form.cleaned_data['ifshare']
            #p_id=id;

            trip = Trip(destination=destination, arrivaltime=arrivaltime, passenger=passenger, ifshare=ifshare,p_id=id)
            #t_id=trip.id
            trip.save()
            messages.success(request, 'Your driver is on the way!')
            #return render(request, 'myuser/optpsg.html', {'form': form, 'trip':trip, 'user': user})
            return HttpResponseRedirect(reverse('myuser:optpsg', args=(user.id,trip.id)))
              
    else:
        form = TripForm()
    return render(request, 'myuser/newtrip.html', {'form': form, 'user': user})

@login_required
def optpsg(request, id,tid):
    user = get_object_or_404(User, id=id)
    trip = get_object_or_404(Trip, id=tid)
    
    if request.method == 'POST':
        form = TripForm(request.POST)

        if form.is_valid():
            trip.destination = form.cleaned_data['destination']
            trip.arrivaltime = form.cleaned_data['arrivaltime']
            trip.passenger = form.cleaned_data['passenger']
            trip.ifshare = form.cleaned_data['ifshare']
            trip.p_id = id

            #trip = Trip(destination=destination, arrivaltime=arrivaltime, passenger=passenger, ifshare=ifshare, p_id=p_id)
            trip.save()
            messages.success(request, 'Your have changed your trip information!')
            return HttpResponseRedirect(reverse('myuser:optpsg', args=[user.id,trip.id]))
    else:
        context = {
            'destination': trip.destination,
            'arrivaltime': trip.arrivaltime,
            'passenger': trip.passenger,
            'ifshare': trip.ifshare,
            'p_id':trip.p_id,
        }
        form = TripForm(context)
    return render(request, 'myuser/optpsg.html', {'form': form, 'user': user, 'trip':trip})

#@login_required
#def optpsg(request,p_id):
#    #trip = get_object_or_404(Trip, id=p_id)
#    return render(request, 'myuser/optpsg.html',)

@login_required
def tripava(request, id):
    user = get_object_or_404(User, id=id)
    userprofile = get_object_or_404(UserProfile, user=user)
    suitable = Trip.objects.filter(Q(vehicle=userprofile.vehicle,status='open') | Q(vehicle='',status='open'))
    #suitable = Trip(vehicle=userprofile.vehicle, status='open') | Trip (vehicle= '',status='open')
    temp = list(suitable)
    avatrip = []
    for trip in temp:
        num = trip.passenger
        if num <= userprofile.capacity:
            avatrip.append(trip)
    
    return render(request, 'myuser/tripava.html', {'user': user, 'userprofile': userprofile, 'avatrip': avatrip})
    
    
@login_required
def confirmtrip(request,id, tid):
    user = get_object_or_404(User, id=id)
    trip = get_object_or_404(Trip, id=tid) 
    if trip.status == 'open':
        trip.status = 'confirmed'
        trip.d_id=id
        trip.save()

    return render(request, 'myuser/confirmtrip.html', {'user':user,'trip':trip})


#@login_required
#def managepage(request, id):
#    user = get_object_or_404(User, id=id)
#    userprofile = get_object_or_404(UserProfile, user=user)
#    
#    tripasdriver= Trip.objects.filter(d_id=id)
#    tripaspsg= Trip. objects.filter(p_id=id)
#    
#    return render(request, 'myuser/profile.html', {'user': user, 'userprofile':userprofile,'tripasdriver':tripasdriver,'tripaspsg':tripaspsg})



@login_required
def optdriver(request, id,tid):
    user = get_object_or_404(User, id=id)
    trip = get_object_or_404(Trip, id=tid)
    
    if request.method == 'POST':
        form = TripForm(request.POST)

        if form.is_valid():
            trip.destination = form.cleaned_data['destination']
            trip.arrivaltime = form.cleaned_data['arrivaltime']
            trip.passenger = form.cleaned_data['passenger']
            trip.ifshare = form.cleaned_data['ifshare']
            trip.d_id = id

            #trip = Trip(destination=destination, arrivaltime=arrivaltime, passenger=passenger, ifshare=ifshare, p_id=p_id)
            trip.save()
            messages.success(request, 'Your have changed your trip information!')
            return HttpResponseRedirect(reverse('myuser:optdriver', args=[user.id,trip.id]))
    else:
        context = {
            'destination': trip.destination,
            'arrivaltime': trip.arrivaltime,
            'passenger': trip.passenger,
            'ifshare': trip.ifshare,
            'd_id':trip.d_id,
        }
        form = TripForm(context)
    return render(request, 'myuser/optpsg.html', {'form': form, 'user': user, 'trip':trip})
    
@login_required
def changeemail(request,id):
    user= get_object_or_404(User,id=id)
    userprofile = get_object_or_404(UserProfile, user=user)
    if request.POST:
       form=UserEmailForm(request.POST)
       if form.is_valid():
          userprofile.email = form.cleaned_data['email']
          userprofile.save()
       return HttpResponseRedirect(reverse('myuser:profile', args=[user.id]))
    else:
       form=UserEmailForm()
    return render(request,'myuser/changeemail.html',{'user':user,'form':form})










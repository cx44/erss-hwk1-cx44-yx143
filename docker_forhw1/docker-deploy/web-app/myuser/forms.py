from django import forms
from django.contrib.auth.models import User
import re;
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.postgres.fields import ArrayField

def valid_email(email):
    pattern = re.compile(r"\"?([-a-zA-Z0-9.`?{}]+@\w+\.\w+)\"?")
    return re.match(pattern, email)

class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=40)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)


        
        
class RegForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50,help_text='username should be less than 50 characters')
    first_name = forms.CharField(label='Firstname', max_length=40,help_text='first name should be less than 40 characters')
    last_name = forms.CharField(label='Lastname', max_length=40,help_text='last name should be less than 40 characters')
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

   


class DriverForm(forms.Form):
    vehicle = forms.CharField(label='vehicle type', max_length=20)
    plate = forms.CharField(label='plate', max_length=10)
    capacity = forms.IntegerField(label='capacity', validators=[MaxValueValidator(100),MinValueValidator(1)])
   

 

class TripForm(forms.Form):
    destination = forms.CharField(label='destination', max_length=50)
    arrivaltime = forms.DateTimeField(label='arrivaltime', help_text=' e.g.: 12/27/2018 16:17')
    passenger = forms.IntegerField(label='number of passengers', validators=[MaxValueValidator(100),MinValueValidator(1)])
    ifshare = forms.BooleanField(label='willing to share?', required=False)
    
    
class UserEmailForm(forms.Form):
    email = forms.EmailField(label='email')
    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email



from django.urls import path
from . import views

app_name='myuser'
urlpatterns = [
    path('', views.home, name='home'),  ##leave as empty first argument if you want it to be home page
    #path('register/', views.register, name='register'),
    #path('login/', auth_views.LoginView.as_view(template_name='myuser/login.html'), name='login'),
    #path('logout/',  views.logout, name='logout'),
 path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('<int:id>/', views.profile, name='profile'),
    path('logout/', views.logout, name='logout'),
    path('<int:id>/driverreg/', views.driverreg, name='driverreg'),
    path('<int:id>/editdriver/', views.editdriver, name='editdriver'),
    path('<int:id>/newtrip/', views.newtrip, name='newtrip'),
    path('<int:id>/<int:tid>/optpsg/', views.optpsg, name='optpsg'),
    path('<int:id>/tripava/', views.tripava, name='tripava'),
    path('<int:id>/<int:tid>/confirmtrip/', views.confirmtrip, name='confirmtrip'),
    path('<int:id>/<int:tid>/optdriver/', views.optdriver, name='optdriver'),
    path('<int:id>/changeemail/', views.changeemail, name='changeemail'),
    #path(r'myuser\\/(?P<id>[0-9]+)\\/(?P<p_id>[0-9]+)\\/optpsg\\/$/', views.optpsg, name='optpsg'),
]
